#!/usr/bin/env python
import gitlab
from gitlab.v4.objects import Project
from jira import JIRA
import json
from datetime import datetime, date


# GitLab config
GITLAB_PROJECT = 'Meters'
GITLAB_TOKEN = 'JXf7LxGcb8_5DkYL71yM'
GITLAB_TOKEN2 = 'REP6t9uxKmzLnHZ8gxt5'
GITLAB_URL = 'https://gitlab.com/'
GITLAB_PROJECT_ID = 30185936
GITLAB_PMETERS_DEV = 31233960
GITLAB_PMETERS_INDEV = 30260233
GITLAB_PMETERS_ST = 30261422
GITLAB_MILESTONE = 2270105
GITLAB_MILESTONE_DEV = 2280264
GITLAB_MILESTONE_INDEV = 2280268
GITLAB_USER = 'santos.castane@wslw.es'
GITLAB_PSW = 'Fuv.35.bDzBjbLj'
GITLAB_IID = None
VERIFY_SSL_CERTIFICATE = True

# Fill out URL, PROJECT, USER, PASS
# JIRA config
JIRA_URL = 'https://jira.edmi.co.uk:8443/'
JIRA_PROJECT = 'CPL'
JIRA_USER = 'manuel.moreno'
JIRA_PASSWORD = 'WslwMM1709'
JIRA_LABELS = []


def createTask(pissue):
    gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN, http_username=GITLAB_USER, http_password=GITLAB_PSW)
    gl.auth()
    project = gl.projects.get(GITLAB_PMETERS_DEV)
    for component in  pissue.fields.components:
            if("Test Framework" in component.name or "TEST FRAMEWORK" in component.name or
             "TEST FRAMEWORK JAMA" in component.name or "TEST FRAMEWORK Sequence" in component.name or "Test Framework Sequence" in component.name):
                project = gl.projects.get(GITLAB_PMETERS_INDEV)              
                break
                
    try:
        issue = project.issues.create({"title": "[" + pissue.key + "] " + pissue.fields.summary,
                               "type": "ISSUE",
                               "created_at": pissue.fields.created,
                               "iid": pissue.id,
                               "state": pissue.fields.status.name,
                                "labels": ['JIRA','No Sprint', 'NoAssignee', "Status::"+pissue.fields.status.name]
                               })
        #print(issue)
    except:
        print("error creando")


def updateTask(pissue,gissue):
    toMove=False;
    gissue.updated_at=pissue.fields.updated
    gissue.title = "[" + pissue.key + "] " + pissue.fields.summary
    if(pissue.fields.resolution):
        gissue.state_event = 'close'
        try:
            datei = pissue.fields.resolutiondate
            dateissue = datetime.strptime(datei,'%Y-%m-%dT%H:%M:%S.000%z')
            dateref = datetime(2021, 7, 12, 00, 00, 00)
            
            if(dateissue.timestamp()>dateref.timestamp()):
                if(gissue.project_id==GITLAB_PMETERS_DEV):
                    gissue.milestone_id=GITLAB_MILESTONE_DEV
                else:
                    gissue.milestone_id=GITLAB_MILESTONE_INDEV
        except Exception as e:
            print('Exception occurred while code execution: ' + str(e))

    try:
        notes = gissue.notes.list()
    except:
        gissue.notes.create({'body': "[Jira Link](https://jira.edmi.co.uk:8443/browse/"+pissue.key+")"})

    try:
        createNewNote = True
        createSecondNote = True
        for note in notes:
            if("SCRUM comment" in note.body):
                note.body = "SCRUM comment: "+pissue.fields.customfield_11400
                note.save()
                createNewNote = False
            elif ("Jira Link" in note.body):
                createSecondNote = False

        if(createNewNote):
            gissue.notes.create({'body': "SCRUM comment: "+pissue.fields.customfield_11400})
    except:
        pass
    try:
        if (createSecondNote):
            gissue.notes.create({'body': "[Jira Link](https://jira.edmi.co.uk:8443/browse/"+pissue.key+")"})      
    except:
        pass

    gissue.labels = []
    try:
        for sprint in  pissue.fields.customfield_10005:
            gissue.labels = []
            if("2021_S6_C0" in sprint):
                gissue.labels.append("2021_S6_C0")
                if(gissue.project_id==GITLAB_PMETERS_DEV):
                    gissue.milestone_id=GITLAB_MILESTONE_DEV
                else:
                    gissue.milestone_id=GITLAB_MILESTONE_INDEV
            # elif ("2021_S6_C0" in sprint ):


            else:
                gissue.labels.append("No Sprint")

    except:
        gissue.labels = ["No Sprint"]
    try:
        if(pissue.fields.assignee):
            gissue.labels.append("Jira User::"+pissue.fields.assignee.name)
        else:
            gissue.labels.append("NoAssignee")
    except:
        gissue.labels = [gissue.labels[0], "NoAssignee"]

    try:
        gissue.labels.append("Status::"+pissue.fields.status.name)

    except:
        gissue.labels = [gissue.labels[0], gissue.labels[1], "Status::NEW"]
    
    try:
        for component in  pissue.fields.components:
            gissue.labels.append(component.name)  
#        for component in  pissue.fields.components:
#            if("Test Framework" in component.name or "TEST FRAMEWORK" in component.name or
#             "TEST FRAMEWORK JAMA" in component.name or "TEST FRAMEWORK Sequence" in component.name or "Test Framework Sequence" in component.name):
#                if(gissue.project_id ==GITLAB_PMETERS_DEV):
#                    toMove=True               
#                    break
            
        

                       

    except:
       pass

    try:
        for label in  pissue.fields.labels:
            gissue.labels.append(label)  
    except:
        pass
    gissue.labels.append("JIRA");  
    index =pissue.key.find("-");
    gissue.labels.append(pissue.key[0,index]);

#    if (toMove == False and gissue.project_id ==  GITLAB_PMETERS_INDEV ):
#           gissue.delete()
#    elif (toMove == True and gissue.project_id ==GITLAB_PMETERS_DEV):
#          gissue.delete()
#    else:
        try:
            gissue.save()
        except:
            print("error saving")

  #  print(gissue)



jira = JIRA('https://jira.edmi.co.uk:8443/', basic_auth=(JIRA_USER, JIRA_PASSWORD))

gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN, http_username=GITLAB_USER, http_password=GITLAB_PSW)
gl.auth()
projectDev = gl.projects.get(GITLAB_PMETERS_DEV)
projectInDev = gl.projects.get(GITLAB_PMETERS_INDEV)
gissuesDev = projectDev.issues
gissuesInDev = projectInDev.issues

cont = 0
jql ="(key in (HW-1175, HW-1201, HW-1237, HW-1339, HW-1340, HW-1371, HW-1372, HW-1402, HW-1403, HW-1348, HW-1401, HW-1400, EM-1703, EM-1671, HW-1241, HW-1252, HW-1253, HW-1254, HW-1338, LTEM-1545, LTEM-1543, HW-1380, HW-1319, HW-1375, HW-1373, EM-1712) or assignee changed TO 'raul.nunez' DURING (2021-11-08, currentLogin())) and resolution = unresolved order by key asc"
while issues_chunk := jira.search_issues(jql, startAt=cont,maxResults=100):
    for issue in issues_chunk:
        cont+=1
        pissue = issue
        try:
            gissue = gissuesDev.get(pissue.id)
            updateTask(pissue,gissue)
        except:
            try:
                gissue = gissuesInDev.get(pissue.id)
                updateTask(pissue,gissue)
            except:
                createTask(pissue)
        print("Complete: " +str(cont)+"/"+ str(issues_chunk.total))



